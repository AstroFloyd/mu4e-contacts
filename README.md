# mu4e-contacts : Contacts Management for the Emacs email system mu4e, using org-mode and helm

This is an Emacs Lisp project that provides a contact management for the email program
[mu4e](https://www.djcbsoftware.nl/code/mu/mu4e.html) by Dirk-Jan C. Binnema. It uses
[helm](https://github.com/emacs-helm/helm) by Thierry Volpiatto and others and was strongly inspired by
[org-contacts.el](https://code.orgmode.org/bzg/org-mode/raw/master/contrib/lisp/org-contacts.el) by Julien Danjou and by
[helm-mu](https://github.com/emacs-helm/helm-mu) by Titus von der Malsburg.

# Features

## Storage

The contact data are stored in plain text format in one or several `org-mode` files. Their format is closely inspired by
that of `org-contacts.el`. A simple private address looks as follows.

```
* Headline

** Sub Headline

*** Duck, Donald :FICTIONAL:

	:PROPERTIES:
	:TYPE: Home
	:LAST: Duck
	:FIRST: Donald
	:PHONE: +1-234-5678901
	:EMAIL: donald@disney.com
	:END:

```

while a more detailed entry, providing both a home and a work address, may look like this:

```
*** Sample, Stephen X. :BUSINESS:REMINDER:

	:PROPERTIES:
	:PREFIX: Dr.
	:FIRST: Stephen
	:MIDDLE: Xavier
	:LAST: Sample
	:SUFFIX: MD
	:NICKNAME: Sweetie
	:BIRTHDAY: 1981-01-03
	:NOTE: Don't forget his birthday.
	:MOBILE: +44-109-472183
	:END:

**** Home

	:PROPERTIES:
	:TYPE: Home
	:STREET: 11 Beach Avenue
	:CITY: Cardiff
	:POSTCODE: CF2 XY3
	:PROVINCE: Wales
	:COUNTRY: United Kingdom
	:EMAIL: stephen@stephen.co.uk
	:PHONE: +44-29-1234567
	:END:

**** Work

	:PROPERTIES:
	:TYPE: Work
	:LINE1: Emergency Department
	:LINE2: St. Thomas Hospital
	:STREET: 51 High Street
	:CITY: Cardiff
	:POSTCODE: CF1 AB2
	:PROVINCE: Wales
	:COUNTRY: United Kingdom
	:COMPANY: St. Thomas Hospital
	:POSITION: Head Surgeon
	:EMAIL: sample@st-thomas.co.uk
	:PHONE: +44-29-98765-0
	:FAX:   +44-29-98765-11
	:URL:   http://www.st-thomas.co.uk/neurology/sample
	:END:
```

The entries `Home` and `Work` here inherit most properties and all tags from their parent entry `Sample, Stephen
X.`. Only entries that have a `TYPE` are valid contacts and will be seen by `mu4e`, i.e. we are defining two contacts
here. Note that the phone and fax numbers depend on whether you want to contact Stephen at home or at work, but he uses
the same mobile phone number in either case. Property inheritance in `org-mode` makes it possible. This way, most
redundancy can be avoided.

Two individuals are considered equal if and only if their first, middle and last names and their IDs match. If there are
thus two distinct people in the data base whose names agree, we add an `ID` property in order to distinguish them:

```
* Headline

** Sub Headline

*** John Smith
	:PROPERTIES:
	:FIRST: John
	:LAST: Smith
	:ID: 1
	:TYPE: Work
	:EMAIL: gardening@sthumphrys.edu
	:NOTE: The Head Gardener at St. Humphry's College
	:END:

*** John Smith
	:PROPERTIES:
	:FIRST: John
	:LAST: Smith
	:ID: 2
	:TYPE: Work
	:EMAIL: trombone@cso.com
	:NOTE: The first trombonist at Chicago Symphony Orchestra
	:END:

```

When `mu4e` is started or when some other function operating on contacts is called for the first time, all the contact
files are read and parsed, and the contacts are stored in an internal lisp variable. As soon as you use Emacs to write
to some of the contact files, the relevant file is automatically parsed again and the internal lisp variable updated
accordingly.

If you modify some of the contact files from outside Emacs (i.e. from outside the instance of Emacs that runs `mu4e`),
you can call `mc-reload-database` in order to update the internal data base.

This functionality is obtained by adding `mc-save-hook` to the list of `after-save-hook`s.

## Email address autocompletion

When you compose a message (`mu4e-compose-mode`) and hit the `[Tab]` key in one of the address fields, this invokes
`mu4e`'s address autocompletion. Usually the candidates for the completion are extracted from `mu`. We extend the
autocompletion in such a way that email addresses from the contacts data base are offered first and those extracted from
`mu` last.

## Helm insertion of email addresses

There is a function `mc-helm-email-insert` that uses `helm` to select one or several email contacts and inserts their
respective email addresses in the form `Prefix Frist Middle Last (Suffix) <name@domain.com>` at the cursor
position. With a suitable key binding, one can invoke it inside the address fields of an email and thereby use a helm
based contact selection rather than the old-fashioned `[Tab]` autocompletion.

Whereas the usual autocompletion produces strings of the form `Prefix First Middle Last (Suffix) <email>`, the helm
source uses `Prefix First (Nickname) Middle Last, Suffix <email> :TAGS:`, and so one can interactively search for
nicknames and tags as well. Helm is switched to 'fuzzy' matching so that typing `Last :TAG:` will find contacts with the
given last name that have the requested tags.

The suggested email contacts are structured in three helm sources: first the contacts from the data base, then the contacts that `mu` extracts from all personal emails and finally the contacts that `mu` extracts from all emails.

## Helm browsing of email contacts

The function `mc-helm-email-menu` browses the email contacts in the same fashion, but provides further actions:
- compose an email to the selected addresses
- insert the selected email addresses at the cursor position
- find all emails that have one of the selected addresses in one of their address fields (`mu` search for `contact:`)
- find all emails that have one of the selected addresses in their `from` or `to` fields
- jump to the definition of the contact in the respective `org-mode` file
- export vCards for all selected contacts to a `.vcf` file.

## Helm browsing of all contacts

The function `mc-helm-full-menu` browses all contacts from the data base, including those that may not specify an email address. It displays these contacts in a compact multi-line format.

## Data export

The functions `mc-export-vcf` and `mc-export-vcf-tags` export all contacts in vCard format, in the latter case filtered
for user selected tags.

The functions `mc-browse-xmlfile` and `mc-browse-xmlfile-tags` export contacts in XML form and display them in the web
browser. The XML file is rendered by an XSL transformation whose style sheet is supplied with this package. As of the
current version, this is at the stage of a proof of principle, but does not look any beautiful yet.

## Contacts capture

There is a new capture template in order to capture a new contact and add it under the heading `New Contacts` in a
specific org-mode file.

# Configuration

In order to load `mu4e-contacts`, you place the files `mu4e-contacts.el` and all `mu4e-contacts-xml-<something>.xml` in the
load-path and
```
(require 'mu4e-contacts)
```

An example configuration with suggested key bindings is as follows,
```
;; All contact files are listed here.
(setq mc-contacts-files '("~/agenda/contacts.org" "~/work/agenda/contacts.org"))

;; The file to which new contacts are added by the capture template.
(setq mc-capture-file "~/agenda/contacts.org")

;; The directory to put exported files.
(setq mc-contacts-export-dir "~")

;; The new capture template is invoked by [o] from the capture templates menu.
(mc-capture-template-bind "o")

;; Key binding to directly invoke the new capture template.
(define-key global-map (kbd "C-c o") (lambda () (interactive) (org-capture nil "o")))

;; Key binding to invoke the contact browser.
(define-key global-map (kbd "C-c b") 'mc-helm-full-menu)

;; Only email addresses in messages after this date are taken into account.
(setq mc-compose-complete-only-after "2015-01-01 00:00:00")

;; The function mu4e-headers-change-sorting used to be bound to [O]. We rebind it to [o] here.
(define-key mu4e-headers-mode-map "o" 'mu4e-headers-change-sorting)
(define-key mu4e-view-mode-map "o"    'mu4e-headers-change-sorting)

;; The helm browsing of email contacts now gets bound to [O].
(define-key mu4e-main-mode-map "O"    'mc-helm-email-menu)
(define-key mu4e-headers-mode-map "O" 'mc-helm-email-menu)
(define-key mu4e-view-mode-map "O"    'mc-helm-email-menu)

;; The helm insertion of email contacts is bound to [C-TAB].
(define-key mu4e-compose-mode-map (kbd "<C-tab>") 'mc-helm-email-insert)
```

# Internals

## Name space

All variables and functions defined by `mu4e-contacts` use the prefix `mc-`.

## Modifications of existing mu4e functions

In order to enhance `mu4e`'s address autocompletion, one of its functions is brutally overwritten. This is
`mu4e~fill-contacts` before version 1.4 and `mu4e~update-contacts` since. Should `mu4e`'s behaviour change in a future
version, there is no guarantee that our modifications will continue to work.

## Parsing org-mode files

When contacts are loaded from an `org-mode` file, all tags and the properties listed in `mc-inherited-properties` are
inherited from the parent entry(ies). Then all entries without a `TYPE` property are discarded, and all properties that
have empty strings as their values removed.

The user is responsible for the structure of the contacts files. There are no consistency checks done.

## Internal representation

The result of the parsing is stored in an internal data base `mc-database-data`. The full paths of all org-mode files
that are contained in this data base is kept track of in `mc-databse-files`. The current data base structure can be
inspected by interactively calling `mc-dump-database` which dumps the contents of `mc-database-data` into a temporary
buffer for inspection.

The `mc-database-data` is a list of data structures, one for each contact (i.e. for each entry that has a `TYPE`
property). The data structure for a contact is an alist that contains the key value pairs that describe its properties.
In addition to all properties from the `:PROPERTIES:` drawers in the `org-mode` files, the following keys are
automatically added:
- `TAGS`: the tags of the entry (added by the `org-mode` parser)
- `ALLTAGS`: all tags including the inherited ones (added by the `org-mode` parser)
- `FILE`: the file from which the entry orignates (added by the `org-mode` parser)
- `POINT`: the position in that file at which the entry is located (added by us)

It is the responsibility of the user not to interfere with these properties. As of the current version, compliance is
not enforced.

## Indexing and Representation

Internally, individuals are identified by a key which is a string of the form `Last|First|Middle|ID` using their first,
middle and last name as well as the explicit ID. This string also defines the sorting of contacts whenever
applicable. For each such individual, there may be one or several contacts, depending on how many entries there are that
have a `TYPE` property and that share the same key `Last|First|Middle|ID`.

Note that once an individual is uniquely identified, the properties `PREFIX` and `SUFFIX` (of her or his name) as well
as `BIRTHDAY`, `NICKNAME` and `NOTE` ought to be unique, too. As of this version, however, no consistency checks are
performed.

## Tasks and Suggestions

### Manadatory

- in order to cleanly unload this module, we need to remove the hook
- when the hook is called, also reread the autocompletion suggestions for the two `mu4e` functions that we modified
- issue error messages when one of the executables is missing, test for existence right at the beginning
- when calling the contacts browser `mc-helm-full-menu` or an actions of it without `mu4e` running, then either issue an error message or start `mu4e` first
- issue error messages when calling a helm action that has insufficient data (i.e. inserting an email of someone who does not have any)
- replace `mc-xml-quote` with a professional solution

### Optional

- rather than overwrite two functions of `mu4e`, we might be able to simply bind the [TAB] key pressed in the email header to one of our functions
- when given a vCard `.vcf` file, go through all its contacts and interactively, either add a contact or merge its data with an existing contact
- for each `mu4e` identity, specify one contact as `self` and offer to attach your own `.vcf` vCard to outgoing messages (with the signature perhaps)
- select contacts, produce one or several temporary vCard `.vcf` files and attach them to the message being composed (call this `mc-attach-contacts`)
- when exporting to XML, offer to choose an XSLT style with a CSS style and open in the browser
- mimick Outlook's printing of a single or a list of contacts by exporting to XML, styling with XSLT and printing in the browser
   (XSLT alternatives: most compact list of all contacts displaying all data; pretty-printed list of contacts; one-contact-one-page)
- remove hard-coded file names of XSLT styles, rather offer a directory and select from these files using helm
- upon saving a contacts file, do not immediately re-import it into the contacts data base, but rather flag it as touched and import it only when a contact
  function is called (modify `mc-load-database` to reload all touched files and remove the touched flags)
- functions for finding contacts using logical 'or' composition of conditions
- in order to sync with a mobile device, export a vCard of all contact with a `:MOBILE:` tag, say, and put a hidden UUID field into the export in order to
  identify the contact when we later import a version of that vCard that was modified on the mobile
- when point is on an email address, either find the corresponding entry in the contacts file or add a new contact with that email (pre-filling LAST, FIRST)
- new action to operate on tags (+:TAG: adds a tag, -:TAG: removes a tag, =:ONE:TWO: sets all tags) of the selected contacts (only for contacts from the data base)
- support for refiling contacts to an archive file (can just use 'org-mode`)

## License

[GNU General Public License GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)
